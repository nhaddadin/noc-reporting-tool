var async = require('async');
var AWS = require('aws-sdk');
var regions = require('../../helpers/regions.js')


function execute(){

    async.each(regions.rds, function(region){

        AWS.config.loadFromPath('./helpers/cred.json');

        var ec2 = new AWS.EC2({region: region});

        var sgData = [];        

        ec2.describeSecurityGroups(function(err, data){

            if(err){
                console.log(err, err.stack);
            } else {
                if(data.SecurityGroups){
                    for(i in data.SecurityGroups){                        

                        if(data.SecurityGroups[i].Description.search('launch-wizard') > -1){
                            sgData.push({
                                sgName: data.SecurityGroups[i].GroupName,
                                sgDesc: 'no'
                            });
                        }
                        else {
                            sgData.push({
                                sgName: data.SecurityGroups[i].GroupName,
                                sgDesc: 'yes'
                            });
                        }   
                    } 
                }

                console.log('---------------------------------------');
                console.log('Security Groups Description Check');
                console.log('This plugin scans the ec2 service in the AWS account to make sure that every security group have a description on it');
                console.log('Report for region: '+region+'\n');
                
                for (i in sgData){
                    
                    if (sgData[i].sgDesc === 'no'){
                        console.log("Security Groups Description Check Failed!\nCouldn't find description for this security group: "+sgData[i].sgName);
                    }
                }
                console.log('---------------------------------------');
            }
        });
    });
}

module.exports = {
    execute: execute,
};