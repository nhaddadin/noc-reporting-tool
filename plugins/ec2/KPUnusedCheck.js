var async = require('async');
var AWS = require('aws-sdk');
var regions = require('../../helpers/regions.js')


function execute(){

    async.each(regions.rds, function(region){

        AWS.config.loadFromPath('./helpers/cred.json');

        var ec2 = new AWS.EC2({region: region});

        var keys = [];

        var instances = [];

        ec2.describeKeyPairs(function(err, data){
            if (err){
                console.log(err, err.stack);
            } else {
                if(data.KeyPairs){
                    for (i in data.KeyPairs){
                        keys.push({
                            keyId: data.KeyPairs[i].KeyName,
                            used: 'no'
                        });
                    }
                }
            
        ec2.describeInstances(function(err, data){
            if (err){
                console.log(err, err.stack);
            } else {
                
                    for (i in data.Reservations){
                        for (j in data.Reservations[i].Instances){
                            instances.push({
                                instanceId: data.Reservations[i].Instances[j].InstanceId,
                                instanceKey: data.Reservations[i].Instances[j].KeyName
                            });
                        }
                    }
                
                for(i in keys){
                    for(j in instances){
                        if(keys[i].keyId === instances[j].instanceKey){
                            keys[i].used = 'yes';
                        }
                    }
                }

                console.log('---------------------------------------');
                console.log('Unused Key Pairs Check');
                console.log('This plugin scans the ec2 service in the AWS account to list the unused key pairs');
                console.log('Report for region: '+region+'\n');

                for(i in keys){
                    if(keys[i].used === 'no'){
                        console.log('Found unused key pair: '+keys[i].keyId);
                    }
                }
                console.log('---------------------------------------');
            }
        });

            }
        });
    });
}

module.exports = {
    execute: execute
};