var async = require('async');
var AWS = require('aws-sdk');
var regions = require('../../helpers/regions.js')


function execute(){

    async.each(regions.rds, function(region){

        AWS.config.loadFromPath('./helpers/cred.json');

        var ec2 = new AWS.EC2({region: region});

        var instancesInfo = [];

        var params = {
            Filters: [
                {
                    Name: 'instance-state-code',
                    Values: ['16']
                },
                {
                    Name: 'platform',
                    Values: ['windows']
                }
            ]      
        };

        ec2.describeInstances(params, function(err, data){
            if(err){
                console.log(err, err.stack);
            } else {

                for(i in data.Reservations){
                    for(j in data.Reservations[i].Instances){
                        for(k in data.Reservations[i].Instances[j].Tags)
                        console.log(data.Reservations[i].Instances[j].Tags[k].Value);                        
                    }
                }

            }
        });

    });

}

module.exports = {
    execute: execute
}