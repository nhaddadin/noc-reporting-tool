var async = require('async');
var AWS = require('aws-sdk');
var regions = require('../../helpers/regions.js')


function execute(){

    async.each(regions.rds, function(region){

        AWS.config.loadFromPath('./helpers/cred.json');

        var ec2 = new AWS.EC2({region: region});

        var instanceData = [];
        var volumesData = [];

        var params = {
            Filters: [
                {
                    Name: 'tag:Name',
                    Values: ['PROD*']
                }
            ]
        };

        ec2.describeInstances(params, function(err, data){
            if(err){
                console.log(err, err.stack);
            } else {
                for(i in data.Reservations){
                    for(j in data.Reservations[i].Instances){
                        for (k in data.Reservations[i].Instances[j].BlockDeviceMappings){
                            instanceData.push({
                                instanceId: data.Reservations[i].Instances[j].InstanceId,
                                volumeId: data.Reservations[i].Instances[j].BlockDeviceMappings[k].Ebs.VolumeId,
                                numOfSnaps: 0
                            });
                        }
                    }
                }

        for (i in instanceData){
            var volId = instanceData[i].volumeId;

            var ebsParams = {
                Filters: [
                    {
                        Name: 'volume-id',
                        Values: [volId]
                    }
                ]
            };

            ec2.describeSnapshots(ebsParams, function(err, data){
                if(err){
                    console.log(err, err.stack);
                } else {
                    for(i in data.Snapshots){
                        volumesData.push({
                            volumeId: data.Snapshots[i].VolumeId,
                            snapId: data.Snapshots[i].SnapshotId
                        });
                    }
    
                    for (i in instanceData){
                        for (j in volumesData){
                            if(instanceData[i].volumeId === volumesData[j].volumeId){
                                instanceData[i].numOfSnaps++;
                            }
                        }
                    }
    
                    console.log('---------------------------------------');
                    console.log('EBS Backup Check');
                    console.log('This plugin scans the ec2 service in the AWS account to make sure that there at least one snapshot of eact ebs attached to a production instance');
                    console.log('Report for region: '+region+'\n');
    
                    for(i in instanceData){
                        if (instanceData[i].numOfSnaps > 0){
                            console.log('Instance ID: '+instanceData[i].instanceId);
                            console.log('Volume ID: '+instanceData[i].volumeId);
                            console.log('Number of Snapshots: '+instanceData[i].numOfSnaps);
                        } else {
                            console.log('EBS Backup Check Faild!\nUnable to find snapshot for this ebs: '+instanceData[i].volumeId+' attached to this instance'+instanceData[i].instanceId);
                        }
                    }
    
                    console.log('---------------------------------------');
    
                }
            });

        }

        

            }
        });                
    });
}

module.exports = {
    execute: execute
};