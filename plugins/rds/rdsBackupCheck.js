var async = require('async');
var AWS = require('aws-sdk');
var regions = require('../../helpers/regions.js')


function execute(){

    async.each(regions.rds, function(region){

        AWS.config.loadFromPath('./helpers/cred.json');

        var rds = new AWS.RDS({region: region});
        

        rds.describeDBInstances(function(err, data) {
			var RDSInstances = [];
            if (err) {
                console.log(err, err.stack);
            } else {
                if (data.DBInstances.length){
                    var RDSInstancesInfo = data.DBInstances;
                    for (i in RDSInstancesInfo) {
                        RDSInstances.push({
                            RDSId: RDSInstancesInfo[i].DBInstanceIdentifier,
                            RDScreateTime: RDSInstancesInfo[i].InstanceCreateTime,
                            SS: 'No',
							SSN: 0
                        });
                    }
                }
            }
			
			rds.describeDBSnapshots(function(err, data) {
				var RDSSnapshots = [];
				if (err) {
					console.log(err, err.stack);
				} else {
					if (data.DBSnapshots.length){
						var RDSSnapshotsInfo = data.DBSnapshots;
						for (i in RDSSnapshotsInfo){
							RDSSnapshots.push({
								RDSId: RDSSnapshotsInfo[i].DBInstanceIdentifier,
								RDSSSId: RDSSnapshotsInfo[i].DBSnapshotIdentifier,
								RDSSScreateTime: RDSSnapshotsInfo[i].SnapshotCreateTime
							}); 
						}
					}
				}
				
				for (i in RDSInstances){
					for (j in RDSSnapshots){
						if (RDSInstances[i].RDSId === RDSSnapshots[j].RDSId){
							RDSInstances[i].SS = 'Yes';
							RDSInstances[i].SSN += 1;
						}
					}
				}

				console.log('---------------------------------------');
				console.log('RDS Backup Check');
				console.log('This plugin scans the RDS service in the AWS account to make sure that there is at least one backup of each RDS instance');
				console.log('Report for region: '+region+'\n');
				
				for (i in RDSInstances){
					if (RDSInstances[i].SS === 'Yes'){
						console.log('RDS Instance ID: ' + RDSInstances[i].RDSId + "\n" + "RDS Instance Creation Date: " + RDSInstances[i].RDScreateTime + "\n" + 'Has a Snapshot: ' + RDSInstances[i].SS + "\n" + 'Number of Snapshots for this Instance: ' + RDSInstances[i].SSN + "\n");
					} 
					if (RDSInstances[i].SS === 'No'){
						console.log("RDS Backup Check Failed!\nCouldn't find RDS Snapshot for this Instance: " + RDSInstances[i].RDSId);
					}
				}
				console.log('---------------------------------------');
			});
        });
    });
}

module.exports = {
    execute: execute,
};