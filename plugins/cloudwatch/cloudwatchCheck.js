var async = require('async');
var AWS = require('aws-sdk');
var regions = require('../../helpers/regions.js')


function execute(){

    async.each(regions.rds, function(region){

        AWS.config.loadFromPath('./helpers/cred.json');

        var cloudwatch = new AWS.CloudWatch({region: region});

        var alarmsData = [];

        cloudwatch.describeAlarms(function(err, data){
            if (err){
                console.log(err, err.stack);
            } else {
                for(i in data.MetricAlarms){
                    alarmsData.push({
                        alarmName: data.MetricAlarms[i].AlarmName,
                        alarmState: data.MetricAlarms[i].StateValue
                    });
                }

                console.log('---------------------------------------');
                console.log('CloudWatch Alarms State Check');
                console.log('This plugin scans the CloudWatch service in the AWS account to make sure that every alarm have a state of Ok');
                console.log('Report for region: '+region+'\n');
                
                for(i in alarmsData){
                    if(alarmsData[i].alarmState !== 'OK'){
                        console.log('CloudWatch Alarms State Check Failed!\nFound CloudWatch Alarm with state of '+alarmsData[i].alarmState+', Alarm Name: '+alarmsData[i].alarmName);
                    }
                }
                
                console.log('---------------------------------------');

            }
        }); 
    });
}

module.exports = {
    execute: execute,
};