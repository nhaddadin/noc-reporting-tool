var async = require('async');
var checks = require('./exports');

async.each(checks.plugins, function(plugin){
    plugin.execute();
});