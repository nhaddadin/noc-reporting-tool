var checks = {
    // rdsBackupCheck          :   require('./plugins/rds/rdsBackupCheck'),
    // SGDescCheck             :   require('./plugins/ec2/SGDescCheck'),
    // KPUnusedCheck           :   require('./plugins/ec2/KPUnusedCheck'),
    // cloudwatchCheck         :   require('./plugins/cloudwatch/cloudwatchCheck'),
    // ebsBackupCheck          :   require('./plugins/ec2/ebsBackupCheck'),
    // elbActivityCheck        :   require('./plugins/elb/elbActivityCheck'),
    ec2InstancesListWindows :   require('./plugins/ec2/ec2InstancesListWindows'),
};

module.exports = {
    plugins: checks
};